import Vue from "vue";
import Vuex from "vuex";
import Flash from "./Flash";
import auth from "./auth.module";
import event from "./event.module";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    flash: new Flash(),
    jurusan: [
      'Manajemen',
      'Ekonomi',
      'Akuntansi',
      'Teknik Sipil',
      'Arsitektur',
      'Administrasi Publik',
      'Administrasi Bisnis',
      'Hubungan Internasional',
      'Hukum',
      'Fisika',
      'Matematika',
      'Informatika',
      'Teknik Elektro',
      'Mekatronika',
      'Teknik Kimia',
      'Teknik Industri',
    ],
  },
  mutations: {},
  actions: {},
  modules: {
    auth,
    event,
  }
});

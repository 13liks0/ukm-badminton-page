import ApiService from "@/common/api.service";
import {
  POST_EVENT, FETCH_EVENT,
} from "./actions.type";
import {
} from "./mutations.type";

const state = {
  events : [],
};

const getters = {
  events: state => state.events,
};

const actions = {
  [FETCH_EVENT](context,data){
    return new Promise((resovle,reject) => {
      ApiService.get("events").then(({data}) => {
        this.state.event.events = data;
      })
    })
  },
  [POST_EVENT](context,data){
    return new Promise((resolve,reject) => {
      ApiService.setHeader();
      var form = new FormData();
      form.append("name", data.name);
      form.append("description", data.description);
      form.append("start", data.start);
      form.append("last", data.last);
      form.append("contact_id", data.contact.id);
      form.append("leader_id", data.leader.id);
      form.append("place", data.location);
      if(data.img != null){
        form.append("img", data.img, data.img.name);
      }
      if(data.id == null){
        ApiService.post("events", form).then(({data}) =>{
          this.state.event.events.push(data);
          this.state.flash.success("Event Created!");
          resolve();
        }).catch(() => {
          this.state.flash.danger("Failed to Create Event!");
          reject();
        })
      } else {
        ApiService.post("events/"+data.id, form).then(({data}) =>{
          this.state.event.events.push(data);
          this.state.flash.success("Event Updated!");
          resolve();
        }).catch(() => {
          this.state.flash.danger("Failed to Update Event!");
          reject();
        })
      }
    })
  }
};

const mutations = {
};

export default {
  state,
  actions,
  mutations,
  getters
};

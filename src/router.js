import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Verify from "./components/Verify.vue";
import ChangePassword from "@/components/ChangePassword";
import NotFound from "./components/NotFound.vue";
import SendToken from "@/components/SendToken";
import Login from "./views/Login.vue";
import Register from "./views/SignUp.vue";
import Profile from "./views/Profile.vue";
import Users from "./views/Users.vue";
import Events from "./views/Events.vue";
import Event from "./views/Event.vue";
import EventCreate from "./views/EventCreate.vue";
import EventUpdate from "./views/EventUpdate.vue";
import User from "./views/User.vue";
Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/signup",
      name: "register",
      component: Register
    },
    {
      path: "/verify",
      name: "verify",
      component: Verify
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile
    },
    {
      path: "/people",
      name: "people",
      component: Users
    },
    {
      path: "/event",
      name: "event",
      component: Events
    },
    {
      path: "/event/:id",
      name: "event-view",
      component: Event
    },
    {
      path: "/event-update/:id",
      name: "event-update",
      component: EventUpdate
    },
    {
      path: "/event-create",
      name: "event-create",
      component: EventCreate
    },
    {
      path: "/people/:id",
      name: "user",
      component: User
    },
    {
      path: "/forget-password",
      component: SendToken,
      props: { isReset: true }
    },
    {
      path: "/resend-verification",
      component: SendToken,
      props: { isReset: false }
    },
    {
      path: "/reset-password",
      component: ChangePassword
    },
    {
      path: "*",
      component: NotFound
    }
  ]
});

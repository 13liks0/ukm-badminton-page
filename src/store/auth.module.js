import ApiService from "@/common/api.service";
import TokenService from "@/common/token.service";
import { URL } from "@/common/config";
import {
  LOGIN,
  LOGOUT,
  REGISTER,
  CHECK_AUTH,
  UPDATE_USER,
  VERIFICATION,
  RESEND_VER_PASSWORD,
  RESEND_VER_EMAIL,
  RESET_PASSWORD,
  FETCH_USER,
  TOGGLE_ADMIN,
} from "./actions.type";
import {
  SET_AUTH,
  PURGE_AUTH,
  SET_ERROR,
  SET_ACCESS_TOKEN,
  VERIFIED
} from "./mutations.type";

const state = {
  errors: null,
  user: {},
  users: [],
  token: {},
  isAuthenticated: !!TokenService.getToken()
};

const getters = {
  users: state => state.users,
  currentUser: state => state.user,
  isAuthenticated: state => state.isAuthenticated
};

const actions = {
  [FETCH_USER](context,data){
    return new Promise((resolve,reject) => {
      ApiService.get("users").then(({data}) => {
        this.state.auth.users = data;
        resolve();
      }).catch(() => {
        reject();
      })
    });
  },
  [RESET_PASSWORD](context,data){
    return new Promise((resolve,reject) => {
      ApiService.post("users/password/reset", data).then(()=>{
        this.state.flash.success("Change password success");
        resolve();
      }).catch(()=>{
        this.state.flash.danger("Failed to change password!");
        reject();
      });
    })
  },
  [RESEND_VER_PASSWORD](context,data){
    return new Promise((resolve,reject) => {
      ApiService.post("users/password/code", data).then(() => {
        this.state.flash.success("Reset link has been sent");
        resolve();
      }).catch(() => {
        this.state.flash.danger("Failed to send link!");
        reject();
      });
    })
  },
  [TOGGLE_ADMIN](context,id){
    return new Promise((reject,resolve) => {
      ApiService.setHeader();
      ApiService.post("users/toggle/"+id.id).then(() => {
        this.state.auth.users.forEach((val,idx) => {
          if(val.id = id.id){
            var admin = this.state.auth.users[idx].adminLevel;
            if(admin == 1){
              this.state.auth.users[idx].adminLevel = 0;
              this.state.flash.success('Add New Admin Success!');
            } else {
              this.state.auth.users[idx].adminLevel = 1;
              this.state.flash.success('Drop Admin Level Success!');
            }
            return;
          }
        })
        resolve();
      }).catch(() => {
        reject();
      });
    });
  },
  [RESEND_VER_EMAIL](context,data){
    return new Promise((resolve,reject) => {
      ApiService.post("users/resend", data).then(() => {
        this.state.flash.success("Verification code has been sent!");
        resolve();
      }).catch(() => {
        this.state.flash.danger("Failed to send code!");
        reject();
      });
    })
  },
  [VERIFICATION](context, data) {
    return new Promise((resolve,reject) => {
      ApiService.setHeader();
      ApiService.post("users/verify", data)
        .then(response => {
          this.state.flash.success("Success to Verify!");
          context.commit(VERIFIED);
          resolve();
        })
        .catch(error => {
          this.state.flash.danger(error.response.data.message);
          reject();
        });
    })
  },
  [LOGIN](context, credentials) {
    return new Promise(resolve => {
      ApiService.post("users/login", {
        username: credentials.username,
        password: credentials.password,
        grant_type: "password"
      })
        .then(({ data }) => {
          context.commit(SET_ACCESS_TOKEN, data);
          resolve(data);

          return new Promise(resolve => {
            ApiService.setHeader();
            ApiService.post("users/details")
              .then(({ data }) => {
                context.commit(SET_AUTH, data);
                resolve(data);
              })
              .catch(({ response }) => {
                context.commit(SET_ERROR, response.data.errors);
              });
          });
        })
        .catch(({ response }) => {
          if (response.data.message_code === "verify") {
            this.state.flash.addLink(
              "Resend verification.",
              "/resend-verification"
            );
            this.state.flash.danger("Please verify your account!");
          } else {
            this.state.flash.danger("Account not exist!");
          }
          context.commit(SET_ERROR, response.data.errors);
          reject();
        });
    });
  },
  [LOGOUT](context) {
    return new Promise((resolve,reject) => {
      ApiService.setHeader();
      ApiService.post("users/logout").then(response => {
        this.state.flash.messages(response.data.message);
        context.commit(PURGE_AUTH);
        resolve();
      });
    })
  },
  [REGISTER](context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.setContent();
      var form = new FormData();
      form.append("fullname", credentials.fullname);
      form.append("username", credentials.username);
      form.append("email", credentials.email);
      form.append("jurusan", credentials.jurusan);
      form.append("bio", credentials.bio);
      form.append("angkatan", credentials.angkatan);
      form.append("password", credentials.password);
      form.append("password_confirmation", credentials.password_confirmation);
      form.append("url", URL+"/verify");
      if(credentials.img != null){
        form.append("img", credentials.img, credentials.img.name);
      }
      ApiService.post("users/register", form)
        .then(({ data }) => {
          context.commit(SET_AUTH, data.user);
          this.state.flash.messages(
            "Register Success! Please Check Your email!"
          );
          resolve(data);
        })
        .catch(({ response }) => {
          this.state.flash.danger("Register Failed!");
          context.commit(SET_ERROR, response.data.errors);
          reject();
        });
    });
  },
  [CHECK_AUTH](context) {
    if (TokenService.getToken()) {
      return new Promise(resolve => {
        ApiService.setHeader();
        ApiService.post("users/details")
          .then(({ data }) => {
            context.commit(SET_AUTH, data);
            resolve(data);
          })
          .catch(({ response }) => {
            context.commit(SET_ERROR, response.data.errors);
          });
      });
    } else {
      context.commit(PURGE_AUTH);
    }
  },
  [UPDATE_USER](context, payload) {
    ApiService.setHeader();
    var form = new FormData();
    form.append("jurusan", payload.jurusan);
    form.append("bio", payload.bio);
    form.append("angkatan", payload.angkatan);
    if(payload.password != ""){
      form.append("password", payload.password);
      form.append("password_confirmation", payload.password_confirmation);
    }
    if(payload.img != null){
      form.append("img", payload.img, payload.img.name);
    }
    return new Promise((resolve,reject) => {
      ApiService.post("users/" + this.state.auth.user.id, form)
      .then(({ data }) => {
        context.commit(SET_AUTH, data.user);
        this.state.flash.messages("Update Success!");
        resolve();
      })
      .catch(() => {
        this.state.flash.danger("Update Failed!");
        reject();
      });
    })
  }
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
  },
  [SET_ACCESS_TOKEN](state, data) {
    TokenService.saveToken(data.access_token);
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    TokenService.destroyToken();
  },
  [VERIFIED](state) {
    state.user.confirmed = true;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};

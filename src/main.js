import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

import { CHECK_AUTH } from "@/store/actions.type";

import App from "./App.vue";
import router from "./router";
import Vuex from "vuex";
import store from "@/store/store";
import ApiService from "./common/api.service";
import { FETCH_USER, FETCH_EVENT } from "./store/actions.type";

ApiService.init();
Vue.use(Vuex);
Vue.config.productionTip = false;
Vue.use(Vuetify);
router.beforeEach((to, from, next) => {
  // console.log(to);
  return Promise.all([store.dispatch(CHECK_AUTH)])
                .then([store.dispatch(FETCH_USER)])
                .then([store.dispatch(FETCH_EVENT)])
                .then(next);
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
